# wpwidget
Create a backbone for widget in wordpress

## Installation
### Via composer
```bash
composer require monkeyscode/widget
```

## Usage
Create a class that extends the core class
```php
class TestWidget extends Widget
{
	public function __construct()
	{
		$widget = [
			'id' => 'test-widget',
			'label' => __('Test Widget'),
			'description' => __('This is test widget')
		];

		$fields = [
			[
		        'label' => 'Message',
		        'name' => 'message',
		        'type' => 'textarea',
			]
		];

		parent::__construct($widget, $fields);
	}
	
    public function handle($instance)
    {
        var_dump($instance);
	}

}

new TestWidget;
```

### Available Fields
See [List of fields](https://github.com/monkeyscode/wpwidget/wiki/List-of-the-Fields) to find out how to use these.

## Contributor
[Duy Nguyen](https://github.com/duyngha)

## Changelogs
[Changelogs](https://github.com/monkeyscode/wpwidget/wiki/Changelogs)
