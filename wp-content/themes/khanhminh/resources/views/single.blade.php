@extends('layouts.full-width')

@section('banner')

    <div class="banner-no-home">
        <img src="{{ asset2('images/banner-trang-trong.jpg') }}">
    </div>

@endsection

@section('content')

    @while(have_posts())
	
		{!! the_post() !!}

        @include('partials.content-single-' . get_post_type())
    @endwhile

@endsection
