    <aside class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 sidebar" data-aos="fade-up" data-aos-delay="800" data-aos-duration="1000">
    	<div class="category-content">
			<?php
				if(is_front_page()) {
					dynamic_sidebar('sidebar-home');
				} else {
					dynamic_sidebar('sidebar-cat');
				}
			?>
        </div>
    </aside>



