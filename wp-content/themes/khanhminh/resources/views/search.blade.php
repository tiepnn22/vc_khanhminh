@extends('layouts.full-width')

@section('banner')

    <div class="banner-no-home">
        <img src="{{ asset2('images/banner-trang-trong.jpg') }}">
    </div>

@endsection

@section('content')

    <div class="page-header">
        <h1>
            <?php _e('Search','khanhminh'); ?>
        </h1>
    </div>

    <h1 class="entry-title">
        <?php _e('Search for :','khanhminh'); ?>
        <?php echo '['.$_GET['s'].']'; ?>
    </h1>

    <div class="page-search">
        <div class="container">
            <div class="page-search-content">
                <div class="category-news">
                    <div class="category-content">
                        <div class="msc-listing">
                            @while(have_posts())
                                
                                {!! the_post() !!}
                                
                                {{ view('partials.content-tin-tuc') }}

                            @endwhile
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ view('partials.pagination') }}

@endsection
