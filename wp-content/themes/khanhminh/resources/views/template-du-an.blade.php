@extends('layouts.full-width')

@section('banner')

    @php
        $pageId = get_the_ID();
        $banner_img = wp_get_attachment_image_src(get_post_thumbnail_id($pageId), 'full');
        $banner_img_check = $banner_img[0];
    @endphp

    <div class="banner-no-home">

        @if (!empty($banner_img_check))
            <img src="{{ $banner_img_check }}">
        @else
            <img src="{{ asset2('images/banner-trang-trong.jpg') }}">
        @endif

    </div>

@endsection

@section('content')
    @while(have_posts())

        {!! the_post() !!}

        @include('partials.page-header')

        <div class="duan">
            @php
                $shortcode = '[listing post_type="duan" layout="partials.content-du-an" paged="yes" per_page="2"]';
                echo do_shortcode($shortcode);
            @endphp
        </div>

    @endwhile

    {!! get_the_posts_navigation() !!}
@endsection


