@extends('layouts.full-width')

@section('banner')

    @php
        $pageId = get_the_ID();
        $banner_img = wp_get_attachment_image_src(get_post_thumbnail_id($pageId), 'full');
        $banner_img_check = $banner_img[0];
    @endphp

    <div class="banner-no-home">

        @if (!empty($banner_img_check))
            <img src="{{ $banner_img_check }}">
        @else
            <img src="{{ asset2('images/banner-trang-trong.jpg') }}">
        @endif

    </div>

@endsection

@section('content')
    @while(have_posts())

        {!! the_post() !!}

        @include('partials.page-header')

        <div class="chuyenvien">
        	<div class="container">
                
                <div class="chuyenvien-meta-title">
                    {{ get_field('ten_doi_ngu') }}
                </div>

	            @php
	                $shortcode = '[listing post_type="chuyenvien" layout="partials.content-chuyen-vien" paged="yes" per_page="4"]';
	                echo do_shortcode($shortcode);
	            @endphp
			</div>
		</div>

    @endwhile

    {!! get_the_posts_navigation() !!}
@endsection


