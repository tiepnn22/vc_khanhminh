<footer class="footer">
    <div class="widget">
        @if (ICL_LANGUAGE_CODE == 'vi')
            {!! get_option('footer_customize_address_vn') !!}
        @else
            {!! get_option('footer_customize_address_en') !!}
        @endif
    </div>
    <div class="widget">
        @if (ICL_LANGUAGE_CODE == 'vi')
            {{ get_option('footer_customize_copyright_vn') }}
        @else
            {{ get_option('footer_customize_copyright_en') }}
        @endif
    </div>
</footer>

<div id="back-to-top">
    <a href="javascript:void(0)">
        <i class="fa fa-chevron-up" aria-hidden="true"></i>
    </a>
</div>
