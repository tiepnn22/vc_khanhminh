<article class="item">

	<figure>
		<a href="{{ $url }}" title="{{ $title }}">
			<img src="{{ asset2('images/3x2.png') }}" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID(), 'box-news-home') }});" />
		</a>
	</figure>

	<div class="title">
		<a href="{{ $url }}">
	    	<h3>{{ $title }}</h3>
	    </a>
	</div>

	{{ view('partials.entry-see-details') }}

</article>


