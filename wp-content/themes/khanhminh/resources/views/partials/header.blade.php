<header class="header">
    <div class="container">
        <div class="header-content">

            <div class="logo">
                <figure>
                    <a href="{!! icl_get_home_url() !!}">
                        <img src="{{ get_option('header_customize_logo') }}">
                    </a>
                </figure>
            </div>

            <div class="header-right">
                <div class="header-top">
                    
                    @if (ICL_LANGUAGE_CODE == 'vi')
                        {{ get_option('header_customize_slogan_vn') }}
                    @else
                        {{ get_option('header_customize_slogan_en') }}
                    @endif

                </div>

                <div class="header-bottom">
                    <nav class="menu">
                        <div class="main-menu">
                            @if (has_nav_menu('main-menu'))
                                {!! wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'menu-primary']) !!}
                            @endif
                        </div>
                        <div class="mobile-menu"></div>
                    </nav>                   
                    <div class="header-bottom-right">
                        <div class="search-box">
                            <form action="{!! esc_url( home_url( '/' ) ) !!}">
                                <input type="text" placeholder="" required requiredmsg="
                                    <?php
                                        if (ICL_LANGUAGE_CODE == 'vi') {
                                            echo 'Bạn vui lòng không để trống ˚¬˚ ';
                                        } else {
                                            echo 'You no empty now ˚¬˚ ';
                                        }
                                    ?>" id="search-box" name="s" value="{!! get_search_query() !!}">
                                <div class="search-icon">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </div>
                            </form>
                        </div>
                        <div class="language">
                            <div class="language-text">
                                <span><?php _e('language','khanhminh'); ?></span>
                            </div>
                            {!! do_action('wpml_add_language_selector') !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</header>




