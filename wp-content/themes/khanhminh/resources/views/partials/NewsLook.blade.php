<div class="msc-listing"> 
	@php
    	$query = new WP_Query(array('showposts' => $number_post, 'meta_key' => 'post_views_count', 'orderby'=> 'meta_value_num', 'order' => 'DESC'));
	@endphp    

	    @while ($query->have_posts())

    		{!! $query->the_post() !!}
	

	        <article class="item">

				<figure>
					<a href="{{ get_permalink() }}">
						<img src="{{ asset2('images/3x2.png') }}" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID(), 'news-sidebar') }});" />
					</a>
				</figure>

				<div class="info">
					<div class="title">
						<a href="{{ get_permalink() }}">
					    	<h3>{{ get_the_title() }}</h3>
					    </a>
					</div>
					<div class="desc">
						<?php
						    if (get_the_excerpt() != '') {
			                    $excerpt = createExcerptFromContent(get_the_excerpt(), 9);
			                } else {
			                    $excerpt = '';
			                }
						?>
						{{ $excerpt }}
					</div>
					<div class="read-more">
						<a href="{{ get_permalink() }}">
							<?php _e('See details','khanhminh'); ?> <i class="fa fa-play-circle-o" aria-hidden="true"></i>
						</a>
					</div>
				</div>

			</article>


		@endwhile
	@php
		wp_reset_postdata();
	@endphp
</div>
