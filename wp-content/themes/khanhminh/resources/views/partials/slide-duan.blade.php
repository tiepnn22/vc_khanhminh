
    <div class="row">
        @php
            $du_an = get_field('anh_du_an');
            
            foreach($du_an as $anh_du_an) {
        @endphp

            <article class="col-12">
                <figure>
                    <a href="{{ get_the_permalink() }}" title="{{ get_the_title() }}">
                        <img src="{{ asset2('images/3x2.png') }}" alt="{{ get_the_title() }}" style="background-image: url({{ $anh_du_an['url'] }});" />
                    </a>
                </figure>
            </article>
            
        @php
            }
        @endphp
    </div>