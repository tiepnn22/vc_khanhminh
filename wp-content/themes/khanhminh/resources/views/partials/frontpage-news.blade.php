
	<article class="item">

		<figure>
			<a href="{{ $url }}">
				<img src="{{ asset2('images/3x2.png') }}" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID(), 'news-sidebar') }});" />
			</a>
		</figure>

		<div class="info">
			<div class="title">
				<a href="{{ $url }}">
			    	<h3>{{ $title }}</h3>
			    </a>
			</div>
			<div class="desc">
				<?php
				    if (get_the_excerpt() != '') {
	                    $excerpt = createExcerptFromContent(get_the_excerpt(), 9);
	                } else {
	                    $excerpt = '';
	                }
				?>
				{{ $excerpt }}
			</div>
			
			{{ view('partials.entry-see-details') }}
		</div>

	</article>


