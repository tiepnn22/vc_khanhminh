<article class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
    <header>
    	<a href="{{ get_permalink() }}">
        	<h2 class="entry-title">
        		{{ get_the_title() }}
        	</h2>
    	</a>
		<!-- @include('partials.entry-meta') -->
    </header>
    <div class="entry-summary">
        {!! the_excerpt() !!}
    </div>
</article>
