<div class="duan-content">
	<div class="container">
		
		<article class="item">

			<div class="title">
				<a href="{{ $url }}" title="">
					<h3>{{ $title }}</h3>
				</a>
			</div>
			
			{{ view('partials.slide-duan') }}

		</article>

	</div>
</div>