
    <article class="item">

        <figure>
            <a href="{{ get_permalink() }}">
                <img src="{{ asset2('images/3x2.png') }}" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID(), 'news') }});" />
            </a>
        </figure>

        <div class="info">
            <div class="title">
                <a href="{{ get_permalink() }}">
                    <h3>{{ get_the_title() }}</h3>
                </a>
            </div>
            <div class="desc">
                @php
                    if (get_the_excerpt() != '') {
                        $excerpt = createExcerptFromContent(get_the_excerpt(), 35);
                    } else {
                        $excerpt = '';
                    }
                @endphp
                {{ $excerpt }}
            </div>
            
            {{ view('partials.entry-see-details') }}
        </div>

    </article>


