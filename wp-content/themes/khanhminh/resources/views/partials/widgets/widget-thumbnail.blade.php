<article class="item">
    <figure>
        <a href="{{ $url }}">
            <img src="{{ asset('images/3x2.png') }}" 
                alt="{{ $title }}" 
                style="background-image: url({{ $thumbnail }});" 
            />
        </a>
    </figure>
    <div class="info">
        <div class="title">
            <a href="{{ $url }}">
                <h3>
                    <?php
                        if (get_the_excerpt() != '') {
                            $excerpt = createExcerptFromContent(get_the_excerpt(), 9);
                        } else {
                            $excerpt = '';
                        }
                    ?>
                    {{ $excerpt }}                    
                </h3>
            </a>
        </div>
        <div class="entry-updated">
            {{ $date }}
        </div>
    </div>
</article>