<article class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 item">

	<figure>
		<img src="{{ asset2('images/3x2.png') }}" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID(), 'partner') }});" />
	</figure>

	<div class="title">
	    <h3>{{ $title }}</h3>
	</div>

</article>


