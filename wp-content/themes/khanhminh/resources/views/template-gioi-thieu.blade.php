@extends('layouts.full-width')

@section('banner')

	@php
        $pageId = get_the_ID();
        $banner_img = wp_get_attachment_image_src(get_post_thumbnail_id($pageId), 'full');
        $banner_img_check = $banner_img[0];
	@endphp

	<div class="banner-no-home">

		@if (!empty($banner_img_check))
			<img src="{{ $banner_img_check }}">
		@else
			<img src="{{ asset2('images/banner-trang-trong.jpg') }}">
		@endif

	</div>

@endsection

@section('content')
    @while(have_posts())

		{!! the_post() !!}

        @include('partials.page-header')

        <div class="gioi-thieu">
	        <div class="container">

	        	<div class="single-content gioi-thieu-content">
    				{!! wpautop(the_content()) !!}
		        </div>

		        <div class="gioi-thieu-video">
		        	{{ the_field('video') }}
		    	</div>

		        <div class="chung-nhan">
	        		<div class="cat-title">
	        			<h3>
	        				@php
				        		$label_field = get_field_object('anh_chung_nhan');
				        		echo $label_field['label'];
	        				@endphp
	        			</h3>
	        		</div>

		        	<div class="row">
			        	@php
					        $chung_nhan = get_field('anh_chung_nhan');
					        foreach($chung_nhan as $anh_chung_nhan) {
				    	@endphp
				    		<article class="col-4">
					        	<a data-fancybox="gallery" href="{{ $anh_chung_nhan['url'] }}">
							    	<img src="{{ asset2('images/2x3.png') }}" style="background-image: url({{ wp_get_attachment_image_src($anh_chung_nhan['id'], 'certify')[0] }});" />
								</a>
				    		</article>
						@php
					        }
				    	@endphp				    	
			    	</div>
			    </div>
				    
			</div>
		</div>
        
    @endwhile
@endsection
