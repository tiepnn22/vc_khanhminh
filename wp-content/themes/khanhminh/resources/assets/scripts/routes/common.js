// using this import type for require third 3rd. For example:
// import Wow from 'wow.js';
 
// or you can specify the script file which you want to import
import meanmenu from 'jquery.meanmenu/jquery.meanmenu.js';
import aos from 'aos/dist/aos.js';
import venobox from 'venobox/venobox/venobox.min.js';
import slick from 'slick-carousel/slick/slick.min.js';

export default {
  init() {
    // JavaScript to be fired on all pages


  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
