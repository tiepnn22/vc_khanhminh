<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');

/**
 * Theme support
 */
add_theme_support('post-thumbnails');

function theme_enqueue_style()
{
    wp_enqueue_style(
        'template-style',
        asset('app.css'),
        false
    );
}

function theme_enqueue_scripts()
{
    wp_enqueue_script(
        'template-scripts',
        asset('app.js'),
        'jquery',
        '1.0',
        true
    );

    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

    $params = array(
       'ajax_url' => admin_url('admin-ajax.php', $protocol)
    );

    wp_localize_script('template-scripts', 'ajax_obj', $params);
}

if (!function_exists('themeSetup')) {
    /**
     * setup support for theme
     *
     * @return void
     */
    function themeSetup()
    {
        // Register menus
        register_nav_menus( array(
    		'main-menu' => __('Main Menu', 'khanhminh')
    	) );

        // add_theme_support('menus');
        add_theme_support('post-thumbnails');
        add_image_size('news', 204, 149, true);
        add_image_size('news-sidebar', 149, 149, true);
        add_image_size('partner', 373, 294, true);
        add_image_size('expert', 264, 314, true);
        add_image_size('certify', 277, 388, true);
        add_image_size('box-news-home', 407, 227, true);
    }

    add_action('after_setup_theme', 'themeSetup');
}

if (!function_exists('themeSidebars')) {
    /**
     * register sidebar for theme
     *
     * @return void
     */
    function themeSidebars()
    {
        $sidebars = [           
            [
                'name'          => __('Contact Form', 'khanhminh'),
                'id'            => 'contact-form',
                'description'   => __('contact form', 'khanhminh'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],
            [
                'name'          => __('Contact Map', 'khanhminh'),
                'id'            => 'contact-map',
                'description'   => __('contact map', 'khanhminh'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],
            [
                'name'          => __('Sidebar Home', 'khanhminh'),
                'id'            => 'sidebar-home',
                'description'   => __('sidebar home', 'khanhminh'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<div class="cat-title"><h3 class="widget-title">',
                'after_title'   => '</h3></div>',
            ],
            [
                'name'          => __('Sidebar Cat', 'khanhminh'),
                'id'            => 'sidebar-cat',
                'description'   => __('sidebar cat', 'khanhminh'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<div class="cat-title"><h3 class="widget-title">',
                'after_title'   => '</h3></div>',
            ],                        
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }

    add_action('widgets_init', 'themeSidebars');
}

if (!function_exists('registerCustomizeFields')) {
    function registerCustomizeFields()
    {
        $data = [
            [
                'info' => [
                    'name' => 'header_customize',
                    'label' => 'Header',
                    'description' => '',
                    'priority' => 1,
                ],
                'fields' => [
                    [
                        'name' => 'logo',
                        'type' => 'upload',
                        'default' => '',
                        'label' => 'Logo'
                    ],
                    [
                        'name' => 'slogan_vn',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Slogan_vn'
                    ],
                    [
                        'name' => 'slogan_en',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Slogan_en'
                    ],
                ],              
            ],
            [
                'info' => [
                    'name' => 'footer_customize',
                    'label' => 'Footer',
                    'description' => '',
                    'priority' => 2,
                ],
                'fields' => [
                    [
                        'name' => 'address_vn',
                        'type' => 'textarea',
                        'default' => '',
                        'label' => 'Address_vn'
                    ],
                    [
                        'name' => 'address_en',
                        'type' => 'textarea',
                        'default' => '',
                        'label' => 'Address_en'
                    ],
                    [
                        'name' => 'copyright_vn',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Copyright_vn'
                    ],
                    [
                        'name' => 'copyright_en',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Copyright_en'
                    ],                                    
                ],              
            ],
            [
                'info' => [
                    'name' => 'box_news_home_customize',
                    'label' => 'Box news home',
                    'description' => '',
                    'priority' => 3,
                ],
                'fields' => [
                    [
                        'name' => 'product_news_home',
                        'type' => 'select',
                        'default' => '',
                        'label' => 'Product_news_home',
                        'choices' => getCategories('duan-category')
                    ],
                    [
                        'name' => 'project_news_home',
                        'type' => 'select',
                        'default' => '',
                        'label' => 'Project_news_home',
                        'choices' => getCategories('duan-category')
                    ]                    
                ],              
            ],
        ];

        $customizer = new MSC\Customizer\Customizer($data);
        $customizer->create();
    }

    add_action('init', 'registerCustomizeFields');
}

function getCategories($tax)
{
    global $wpdb;

    $sql = "SELECT * FROM {$wpdb->prefix}terms as terms 
            JOIN {$wpdb->prefix}term_taxonomy as term_tax ON terms.term_id = term_tax.term_id 
            WHERE term_tax.taxonomy = '{$tax}'";

    //$sql = "SELECT term_id FROM {$wpdb->prefix}term_taxonomy WHERE taxonomy='$tax'";

    $results = $wpdb->get_results($sql);

    //var_dump($results);exit;

    $cates = [];

    foreach ($results as $term) {

        $cates[$term->term_id] = $term->name;

    }

    // var_dump($cates);exit;

    return $cates;
}

function custom_text_pagination_listing($pagi){
	$pagi = array(
        'tag'       => 'div',
        'class'     => 'pagination',
        'id'        => '',
        'prev_text' => '«',
        'next_text' => '»'
    );
    return $pagi;
}
add_filter('paged_wrap', 'custom_text_pagination_listing');

