<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class NewsNews extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('newsnews', 'khanhminh'),
            'label'       => __('Latest news', 'khanhminh'),
            'description' => __('Latest news', 'khanhminh'),
        ];

        $fields = [
            [
                'label' => __('Number post', 'khanhminh'),
                'name'  => 'number_post',
                'type'  => 'number',
            ]          
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        $number_post =  $instance['number_post'];
        ?>
            <div class="cat-title">
                <h3>
                    <?php _e('Latest news','khanhminh'); ?>
                </h3>
            </div>
        <?php
        $shortcode = "[listing per_page=$number_post orderby='date' layout='partials.frontpage-news']";
        echo do_shortcode($shortcode);
                            
	}
}
