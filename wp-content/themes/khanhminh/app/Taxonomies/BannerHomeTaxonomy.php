<?php

namespace App\Taxonomies;

use MSC\Tax;

class BannerHomeTaxonomy extends Tax
{
	public function __construct()
	{
		$config = [
			'slug' => 'bannerhome-category',
			'single' => 'BannerHome Category',
			'plural' => 'BannerHome Categories'
		];

		$postType = 'bannerhome';

		$args = [

		];

		parent::__construct($config, $postType, $args);
	}
}