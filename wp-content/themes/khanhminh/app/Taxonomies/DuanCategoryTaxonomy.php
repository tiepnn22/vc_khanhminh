<?php
namespace App\Taxonomies;

use MSC\Tax;

class DuanCategoryTaxonomy extends Tax
{
    public function __construct()
    {
        $config = [
            'slug'   => 'duan-category',
            'single' => 'Duan-Category',
            'plural' => 'Duan-Categories'
        ];

        $postType = 'duan';

        $args = [];

        parent::__construct($config, $postType, $args);
    }
}
