<?php
/**
 * This file create Chuyenvien custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class ChuyenvienCustomType extends CustomPost
{
    /**
     * [$type description]
     * @var  string
     */
    public $type = 'chuyenvien';

    /**
     * [$single description]
     * @var  string
     */
    public $single = 'Chuyên viên';

    /**
     * [$plural description]
     * @var  string
     */
    public $plural = 'Chuyên viên';

    /**
     * $args optional
     * @var  array
     */
    public $args = ['menu_icon' => 'dashicons-location-alt'];

}
