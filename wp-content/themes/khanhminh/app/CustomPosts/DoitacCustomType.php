<?php
/**
 * This file create Doitac custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class DoitacCustomType extends CustomPost
{
    /**
     * [$type description]
     * @var  string
     */
    public $type = 'doitac';

    /**
     * [$single description]
     * @var  string
     */
    public $single = 'Đối tác';

    /**
     * [$plural description]
     * @var  string
     */
    public $plural = 'Đối tác';

    /**
     * $args optional
     * @var  array
     */
    public $args = ['menu_icon' => 'dashicons-location-alt'];

}
